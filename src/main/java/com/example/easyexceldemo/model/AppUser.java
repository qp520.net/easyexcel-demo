package com.example.easyexceldemo.model;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AppUser {

    private Integer id;

    @ExcelProperty(index = 0)
    private String username;

    @ExcelProperty(index = 1)
    private int age;

    @ExcelProperty(index = 2)
    private String address;

    private String phone;

    private int sex;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private int status;

    private int isVip;
}
