package com.example.easyexceldemo.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.example.easyexceldemo.utils.LocalDateTimeConverter;
import lombok.Data;
import java.time.LocalDateTime;

@Data
public class UserDto {

    @ExcelProperty(value = "姓名",index = 0)
    private String username;

    @ExcelProperty(value = "年龄",index = 1)
    private int age;

    @ExcelProperty(value = "地址",index = 2)
    private String address;

    @ExcelProperty(value = "日期",index = 3,converter = LocalDateTimeConverter.class)
    private LocalDateTime createTime;
}
