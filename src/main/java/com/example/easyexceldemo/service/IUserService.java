package com.example.easyexceldemo.service;

import com.example.easyexceldemo.model.AppUser;
import com.example.easyexceldemo.model.UserDto;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    void save(AppUser user);

    void saveBatch(List<AppUser> data);

    List<UserDto> getList(UserDto userDto);

}
