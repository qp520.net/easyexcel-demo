package com.example.easyexceldemo.service.impl;

import com.alibaba.fastjson.JSON;
import com.example.easyexceldemo.model.AppUser;
import com.example.easyexceldemo.model.UserDto;
import com.example.easyexceldemo.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class IUserServiceImpl implements IUserService {

    @Override
    public void save(AppUser user) {

    }

    @Override
    public void saveBatch(List<AppUser> data) {
        log.warn("获得数据：{},写入数据", JSON.toJSONString(data));
    }

    @Override
    public List<UserDto> getList(UserDto userDto) {
        List<UserDto> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            UserDto user = new UserDto();
            user.setUsername("小明");
            user.setAge(Integer.parseInt("1"+i));
            user.setAddress("北京"+i);
            user.setCreateTime(LocalDateTime.now());
            list.add(user);
        }
        return list;
    }
}
