package com.example.easyexceldemo.utils;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
@Slf4j
public class JdbcEventListner<T> extends AnalysisEventListener<T> {
    /**
     * 读取到x条的时候消费掉数据，然后继续
     */
    private static final Integer MAX_SIZE = 3;


    /**
     * 用来消费解析到的excel数据
     */
    private final Consumer<Collection<T>> batchConsumer;

    /**
     * 解析数据的临时存储容器
     */
    private final List<T> list = new ArrayList<>();

    public JdbcEventListner(Consumer<Collection<T>> batchConsumer) {
        this.batchConsumer = batchConsumer;
    }

    /**
     * 发生异常了清空list
     * @param exception
     * @param context
     * @throws Exception
     */

    @Override
    public void onException(Exception exception, AnalysisContext context) throws Exception {
        list.clear();
        throw exception;
    }

    /**
     * 这个函数是解析每一行数据都会调用
     * @param data
     * @param analysisContext
     */
    @Override
    public void invoke(T data, AnalysisContext analysisContext) {
        log.debug("解析到一条数据：{}", JSON.toJSONString(data));
        list.add(data);
        if (list.size()>=MAX_SIZE){
            batchConsumer.accept(list);
            list.clear();
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        batchConsumer.accept(list);
        log.debug("所有数据都已经解析完了！");
    }

}
