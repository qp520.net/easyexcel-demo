package com.example.easyexceldemo.utils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PageUtils<T> {

    //按每3个一组分割
    private static final Integer MAX_NUMBER = 30;


    public  List<List<T>> page(List<T> data){
        int limit= countStep(data.size());
        List<List<T>> splitList = Stream.iterate(0, n -> n + 1).limit(limit).parallel().map(a -> data.stream().skip(a * MAX_NUMBER).limit(MAX_NUMBER).parallel().collect(Collectors.toList())).collect(Collectors.toList());
        return splitList;
    }

    private static Integer countStep(Integer size) {
        return (size + MAX_NUMBER - 1) / MAX_NUMBER;
    }
}
