package com.example.easyexceldemo.service;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.example.easyexceldemo.model.AppUser;
import com.example.easyexceldemo.model.UserDto;
import com.example.easyexceldemo.utils.JdbcEventListner;
import com.example.easyexceldemo.utils.LocalDateTimeConverter;
import com.example.easyexceldemo.utils.PageUtils;
import org.apache.catalina.User;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.convert.converter.ConverterRegistry;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
public class IUserServiceTest {

    @Autowired
    private IUserService userService;

    @Test
   public void readTest() throws Exception{
        Consumer<List<AppUser>> insertData = userService::saveBatch;
        InputStream inputStream = new FileInputStream("C:\\Users\\ylsgq\\Desktop\\Sql WorkBench\\appUser.xlsx");
        ExcelReader excelReader = EasyExcel.read(inputStream, AppUser.class,new JdbcEventListner(insertData)).build();
        ReadSheet readSheet = EasyExcel.readSheet(0).build();
        excelReader.read(readSheet);
//        excelReader.readAll();
        excelReader.finish();
    }

    @Test
    public void writeTest() throws Exception{
        UserDto userDto = new UserDto();
        OutputStream outputStream = new FileOutputStream("C:\\Users\\ylsgq\\Desktop\\Sql WorkBench\\wirte.xlsx");
        Function<UserDto,List<UserDto>> readData = t->userService.getList(t);
        ExcelWriter writer = EasyExcel.write(outputStream,UserDto.class).build();
        // LocalDateTimeConverter 只针对LocalDatetime类型的字段 假如用Date 表示时间 则不需要注册此Converter
//        WriteSheet sheet = EasyExcel.writerSheet("1").registerConverter(new LocalDateTimeConverter()).build();
//        writer.write(readData.apply(userDto),sheet);
        List<UserDto> list = readData.apply(userDto);
        List<List<UserDto>> nList = new PageUtils<UserDto>().page(list);
        for (int i = 0; i < nList.size(); i++) {
           WriteSheet sheet =  EasyExcel.writerSheet(i).build();
           writer.write(nList.get(i),sheet);
        }
        writer.finish();
    }
}